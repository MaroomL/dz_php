
<?php

include 'bootstrap.php';

$db = new mysqli('127.0.0.1', 'root', '', 'library');
mysqli_set_charset($db,"utf8");

$selectAll =  $db->query("SELECT `id`, `BookName`, `Year`, `Author` FROM books");
echo "<table border=\"1\" width=\"100%\" cellpadding=\"5\"><tr><th>Book Title</th><th>Year</th><th>Author</th></tr>";
while ($row = $selectAll->fetch_assoc()) {
    $idOfBook = $row["id"];
    echo '<tr> <th>' . $row["BookName"] . '</th> <th>' . $row["Year"] . '</th> <th>' . $row["Author"] . '</th> <th><a href="show.php?id=' . $idOfBook . '" style="text-decoration: none; color: black;">Show Book</a></th> <th><a href="delete.php?id=' . $idOfBook . '" style="text-decoration: none; color: black;">Delete Book</a></th> </tr>'  ;
}
echo '</table>';
