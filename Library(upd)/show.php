<?php

include 'bootstrap.php';
$id = $_GET["id"];

$db = new mysqli('127.0.0.1', 'root', '', 'library');
mysqli_set_charset($db,"utf8");

$getInfo = $db->query("SELECT `file_path`, `id` FROM `books` WHERE `id` = '$id'");

$allInfo = $getInfo->fetch_assoc();

if ($allInfo["id"] === $id) {
    echo file_get_contents($allInfo["file_path"]);
}
else {
    echo "Book with requested id was not found";
}
