<?php

include 'bootstrap.php';

$id = $_GET["id"];

$db = new mysqli('127.0.0.1', 'root', '', 'library');
mysqli_set_charset($db,"utf8");

$getInfo = $db->query("SELECT `BookName`, `id`, `file_path` FROM `books` WHERE `id` = '$id'");

$allInfo = $getInfo->fetch_assoc();

if ($allInfo["id"] === $id) {
    echo "Book with title " . $allInfo["BookName"] . " has been deleted.";

    $deleteBook = $db->query("DELETE FROM `books` WHERE `id` = '$id'");

    unlink($allInfo["file_path"]);
}

else {
    echo "Book with requested id was not found";
}
