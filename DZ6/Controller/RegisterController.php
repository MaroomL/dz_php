<?php
/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 19.05.2018
 * Time: 13:48
 */

class RegisterController extends Controller {
    public function indexAction () {
        return $this->render('index');
    }

    public function storeAction(Request $request) {
        if (!$request->isPost()) {
            return false;
        }

        $model = new User();
        $model->name = $request->post('name');
        $model->email = $request->post('email');
        $model->password = $request->post('password');
        $model->save();

        $_SESSION['status'] = 'User ' . $request->post('name') . ' has been successfully added';

        Router::redirect('?route=index/index');
    }
}