<?php
/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 19.05.2018
 * Time: 15:38
 */

class IndexController extends Controller {
    public function indexAction() {
        $status = isset($_SESSION['status']) ? $_SESSION['status'] : null;

        unset($_SESSION['status']);

        return $this->render('index', [
            'users' => User::all(),
            'status' => $status
        ]);
    }
}