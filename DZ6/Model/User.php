<?php
/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 19.05.2018
 * Time: 14:37
 */

class User extends AbstractModel {
    protected static $table = 'users';

    /**
     * @var integer;
     */
    public $id;

    /**
     * @var string;
     */
    public $name;

    /**
     * @var string;
     */
    public $email;

    /**
     * @var string;
     */
    public $password;
}