<?php

$file_name = pathinfo($_FILES['file']['name']);
$file_extension = $file_name['extension'];
$file_information_inside = file_get_contents($_FILES['file']['tmp_name']);
if ($file_extension !== 'json' && $file_extension !== 'xml' && $file_extension !== 'png' && $file_extension !== 'xls') {
    echo "Invalid format. Only .json, .xml, .png and .xls are supported";
}
else {
    $name_of_new_file = $_POST['textInput'];
    $open = fopen( $name_of_new_file . "." . $file_extension, "x");
    fwrite($open, $file_information_inside);
    fclose($open);
}