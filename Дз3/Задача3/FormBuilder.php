<?php
/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 13.04.2018
 * Time: 17:41
 */

class FormBuilder {

    public function open($createForm = array(''=>'', ''=>'')){
        return "<form action='" . $createForm["action"] . "'" . "method='" . $createForm["method"] . "'>\r\n" ;
    }

    public function input($createInput = array(''=>'', ''=>'', ''=>'')){
        return "    <input type='" . $createInput["type"] . "' placeholder='" . $createInput["placeholder"] . "' name='" . $createInput["name"] . "'>\r\n";
    }

    public function submit($createButton = array(''=>'')){
        return "    <button type='submit'>" . $createButton["value"] . "</button>\r\n";
    }

    public function close() {
        return "</form>\r\n";
    }
}

$form = new FormBuilder();
echo $form->open(['action'=>'index.php', 'method'=>'POST']);
echo $form->input(['type'=>'text', 'placeholder'=>'Ваше имя', 'name'=>'name']);
echo $form->input(['type'=>'text', 'placeholder'=>'email', 'name'=>'email']);
echo $form->submit(['value'=>'Отправить']);
echo $form->close();