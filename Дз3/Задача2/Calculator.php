<?php
/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 12.04.2018
 * Time: 18:35
 */

class MyCalulator {
    public $a = 0;
    public $b = 0;
    public function __construct($a, $b) {
        $this->a = $a;
        $this->b = $b;
    }

    public function add() {
        return $this->a + $this->b . '<br>';
    }

    public function subtract() {
        return $this->a - $this->b . '<br>';
    }

    public function multiply() {
        return $this->a * $this->b . '<br>';
    }

    public function divide() {
        return $this->a / $this->b . '<br>';
    }
}

$calc = new MyCalulator(12, 5);

echo $calc->add();
echo $calc->subtract();
echo $calc->multiply();
echo $calc->divide();

