<?php
/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 12.04.2018
 * Time: 17:05
 */
class WorkerAlpha {
    public $name = "default_name";
    public $age = 30;
    public $salary = 2000;
}

$firstWorker = new WorkerAlpha();

$firstWorker->name = "Иван";
$firstWorker->age = 25;
$firstWorker->salary = 1000;

$secondWorker = new WorkerAlpha();

$secondWorker->name = "Вася";
$secondWorker->age = 26;
$secondWorker->salary = 2000;

echo "Сума зарплат Ивана и Васи равна: " . ($firstWorker->salary + $secondWorker->salary) . '<br>';
echo "Сума возрастов Ивана и Васи равна: " . ($firstWorker->age + $secondWorker->age) . '<br>';




class WorkerBeta {
    private $name = "default_name";
    private $age = 20;
    private $salary = 2000;

    public function getName()
    {
        return $this->name;
    }

    public function getAge()
    {
        return $this->age;
    }

    public function getSalary()
    {
        return $this->salary;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setAge($age)
    {
        $this->age = $age;
    }

    public function setSalary($salary)
    {
        $this->salary = $salary;
    }

}

$firstWorker = new WorkerBeta();
$secondWorker = new WorkerBeta();

$firstWorker->setName("Иван");
$firstWorker->setAge(25);
$firstWorker->setSalary(1000);

$secondWorker->setName("Вася");
$secondWorker->setAge(26);
$secondWorker->setSalary(2000);

echo "Сума зарплат Ивана и Васи равна: " . ($firstWorker->getSalary() + $secondWorker->getSalary()) . '<br>';
echo "Сума возрастов Ивана и Васи равна: " . ($firstWorker->getAge() + $secondWorker->getAge()) . '<br>';

class WorkerOmega {
    private $name = "default_name";
    private $age = 20;
    private $salary = 2000;

    public function getName()
    {
        return $this->name;
    }

    public function getAge()
    {
        return $this->age;
    }

    public function getSalary()
    {
        return $this->salary;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setAge($age)
    {
        $this->age = $age;
        $this->checkAge();
    }

    private function checkAge() {
        if ($this->age < 1 || $this->age > 100) {
            $this->age = 20;
            echo "Вы ввели неправильный возраст. Проверьте ваши данные и повторите попытку снова. Возраст установлен стандартный." . '<br>';
        }
    }

    public function setSalary($salary)
    {
        $this->salary = $salary;
    }


}

$firstWorker = new WorkerOmega();
$secondWorker = new WorkerOmega();

$firstWorker->setName("Иван");
$firstWorker->setAge(25);
$firstWorker->setSalary(1000);

$secondWorker->setName("Вася");
$secondWorker->setAge(111);
$secondWorker->setSalary(2000);

echo "Сума возрастов Ивана и Васи равна: " . ($firstWorker->getAge() + $secondWorker->getAge()) . '<br>';
