<?php
/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 06.05.2018
 * Time: 11:14
 */

class CurrencyController extends Controller
{
    const API = 'https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5';
    protected function getJson() {
        $bank = file_get_contents(self::API);
        $bank = json_decode($bank);
        return $bank;
    }

    public function indexAction(){
        foreach ($this->getJson() as $item) {
            if ($item->ccy === "USD") {
                $USDpokupka = $item->buy;
                $USDprodazha = $item->sale;
            }
            if ($item->ccy === "EUR") {
                $EURpokupka = $item->buy;
                $EURprodazha = $item->sale;
            }
            if ($item->ccy === "BTC") {
                $BTCpokupka = $item->buy;
                $BTCprodazha = $item->sale;
            }
            if ($item->ccy === "RUR") {
                $RURpokupka = $item->buy;
                $RURprodazha = $item->sale;
            }
        }
        return $this->render('index', [
            'USDpokupka' => round($USDpokupka, 2),
            'USDprodazha' => $USDprodazha,
            'EURpokupka' => $EURpokupka,
            'EURprodazha' => $EURprodazha,
            'RURpokupka' => $RURpokupka,
            'RURprodazha' => $RURprodazha,
            'BTCpokupka' => $BTCpokupka,
            'BTCprodazha' => $BTCprodazha,
        ]);
    }

    public function converterAction() {
        $status = isset($_SESSION['status']) ? $_SESSION['status'] : null;

        unset($_SESSION['status']);

        return $this->render('converter', [
            'status' => $status
        ]);
    }

    public function calculateAction(Request $request) {
        if (!$request->isPost()) {
            return false;
        }

        $form1 = $request->post('form1');
        $form2 = $request->post('form2');
        $ish = $request->post('ish');

        foreach ($this->getJson() as $item) {
            if ($item->ccy === "USD") {
                $USDpokupka = $item->buy;
                $USDprodazha = $item->sale;
            }
            if ($item->ccy === "EUR") {
                $EURpokupka = $item->buy;
                $EURprodazha = $item->sale;
            }
            if ($item->ccy === "BTC") {
                $BTCpokupka = $item->buy;
                $BTCprodazha = $item->sale;
            }
            if ($item->ccy === "RUR") {
                $RURpokupka = $item->buy;
                $RURprodazha = $item->sale;
            }
        }

        if ($form1 === $form2) {
            $_SESSION['status'] = 'Из ' . $ish . ' ' . $form1 . ' вы получите ' . $ish . ' ' . $form2;
        }
        else {
            switch ($form1) {
                case 'USD':
                    $price1 = $USDpokupka;
                    break;
                case 'EUR':
                    $price1 = $EURpokupka;
                    break;
                case 'RUR':
                    $price1 = $RURpokupka;
                    break;
                case 'BTC':
                    $price1 = $BTCpokupka;
                    break;
            }

            switch ($form2) {
                case 'USD':
                    $price2 = $USDpokupka;
                    break;
                case 'EUR':
                    $price2 = $EURpokupka;
                    break;
                case 'RUR':
                    $price2 = $RURpokupka;
                    break;
                case 'BTC':
                    $price2 = $BTCpokupka;
                    break;
            }

            $rez = ($price1 / $price2) * $ish;

            $_SESSION['status'] = 'Из ' . $ish . ' ' . $form1 . ' вы получите ' . $rez . ' ' . $form2;
        }
        return header('Location: ?route=currency/converter');
    }
}