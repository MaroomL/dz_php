<?php
/**
 * @author Dima Korets korets.web@gmail.com
 * @Date: 17.05.18
 */

abstract class AbstractModel
{
    protected static $table = '';
    private static function getDbcon() {
        return DbConnection::getInstance()->getMysqli();
    }
    /**
     * @return bool|mysqli_result
     */
    public static function all()
    {
        $table = static::$table;
        return self::getDbcon()->query("SELECT * FROM {$table}");
    }

    public static function find(integer $id)
    {
        return self::getDbcon()->query("SELECT * FROM {static::$table} WHERE `id` = {$id}");
    }

    public static function last()
    {
        return self::getDbcon()->query("SELECT * FROM {static::$table} ORDER BY `id` DESC LIMIT 1");
    }

    public function update($id)
    {
        $table = static::$table;

        $fields = [];
        $values = [];

        foreach (get_object_vars($this) as $propertyName => $value) {
            if ($propertyName === 'id') {
                continue;
            }
            if ($value === '') {
                continue;
            }
            $fields[] = "`" . $propertyName . "`" . '=' . "'" . $value . "'";
        }

        $fields = implode(', ', $fields);

        return self::getDbcon()->query("UPDATE `{$table}` SET  {$fields} WHERE `id` = {$id}");

    }

    public function delete($id) {
        $table = static::$table;

        return self::getDbcon()->query("DELETE FROM {$table} WHERE `id` = {$id}");
    }


    public function save()
    {
        $table = static::$table;

        $fields = [];
        $values = [];

        foreach (get_object_vars($this) as $propertyName => $value) {
            if ($propertyName === 'id') {
                continue;
            }
            $fields[] = "`" . $propertyName . "`";
            $values[] = "'" . $value . "'";
        }

        $fields = implode(', ', $fields);
        $values = implode(', ', $values);


        return self::getDbcon()->query("INSERT INTO `{$table}` ({$fields}) VALUES ($values)");
    }
}